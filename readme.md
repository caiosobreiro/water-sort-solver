## About

This is an AI project built with one purpose in mind. Solving the "Water Sort Puzzle" android game.


## Solution

- Use OpenCV to detect how many vials there are and where are they (this changes according to the level difficulty).
- Brute force the solution
- Use the ADB/Android monkey the execute the movements

## Important

Disable networking to avoid ads. Otherwise the AI is going to break.