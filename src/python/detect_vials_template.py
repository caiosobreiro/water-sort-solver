import numpy as np
import matplotlib.pyplot as plt
import cv2
import sys
import os
import json
import nms

colorsRGB = {
    repr([234, 94, 123]): 'pink',
    repr([232, 140, 66]): 'orange',
    repr([58, 46, 195]): 'blue',
    repr([85, 163, 229]): 'cyan',
    repr([98, 214, 124]): 'lime',
    repr([197, 42, 35]): 'red',
    repr([99, 100, 101]): 'gray',
    repr([114, 43, 147]): 'purple',
    repr([120, 150, 14]): 'lightgreen',
    repr([241, 218, 88]): 'yellow',
    repr([16, 101, 51]): 'green',
    repr([126, 74, 7]): 'brown'
}

dirname = os.path.dirname(__file__)

templatePath = os.path.join(dirname, '../images/vial_template_full.png')
mainPath = os.path.join(dirname, '../tmp/screencap.png')
outputImagePath = os.path.join(dirname, '../tmp/detected.png')


# Load template, convert to grayscale, perform canny edge detection
template = cv2.imread(templatePath)
template = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
template = cv2.Canny(template, 50, 200)
(tH, tW) = template.shape[:2]

# Load original image, convert to grayscale
original_image = cv2.imread(mainPath)
gray = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)

canny = cv2.Canny(gray, 50, 200)
detected = cv2.matchTemplate(canny, template, cv2.TM_CCORR_NORMED)
threshold = 0.35
loc = np.where( detected >= threshold )

boxes = []
probs = []

for pt in zip(*loc[::-1]):
    start = pt
    end = (pt[0] + tW, pt[1] + tH)
    boxes.append([start[0], start[1], end[0], end[1]])
    probs.append(detected[pt[1]][pt[0]])

nmsOut = nms.non_max_suppression(np.array(boxes, dtype=np.float32), probs, overlapThresh=0)

def getx1y1 (e):
    return e[0] * 10 + e[1] * 100

# sort by x1 and then by y1
sortedBoxes = sorted(nmsOut, key=getx1y1)

vials = []

for box in sortedBoxes:
    [x1, y1, x2, y2] = box
    cv2.rectangle(original_image, (x1, y1), (x2, y2), (0,0,255), 2)

    yoffset = 100
    xc = int((x1 + x2) / 2)
    yc = int((y1 + y2) / 2)
    vial = {
        'content': [],
        'position': []
    }

    for i in range (4):
        x = xc
        y = 120 + y1 + yoffset * i

        color = original_image[y, x]
        color = color.tolist()
        color.reverse()

        if (color[0] == color[1] and color[1] == color[2]):
            continue

        string = repr(color)
        if string in colorsRGB:
            vial['content'].append(colorsRGB[string])
        else:
            vial['content'].append('ERROR')

        cv2.circle(original_image, (x, y), 10, color=(20,220,20), thickness=3)

    # cv2.circle(original_image, (xc, yc), 10, color=(100,100,100), thickness=3)

    vial['position'] = [str(xc), str(yc)]
    vials.append(vial)

cv2.imwrite(outputImagePath, original_image)
# cv2.imwrite('template_canny.png', template)
# cv2.imwrite('canny.png', canny)

print(json.dumps(vials))

# # show the image
# plt.imshow(colorFixed)
# plt.show()