import { uniq, cloneDeep, clone } from 'lodash'
import { Move, Vial, Vials } from './Types'

export function isValid (vials: Vials) {
    const colors = {}
    for (let vial of vials) {
        for (const color of vial) {
            if (!colors[color]) {
                colors[color] = 0
            }
            colors[color]++
            if (colors[color] > 4) {
                return false
            }
        }
    }

    for (let color of Object.keys(colors)) {
        if (colors[color] !== 4) {
            console.log(`Color ${color} was detected ${colors[color]} times`)

            return false
        }
    }
    return true
}

function getAmountTopColor (vial: Vial) {
    const topColor = vial[0]
    let amount = 0
    for (const color of vial) {
        if (color === topColor) {
            amount++
        } else {
            break
        }
    }
    return amount
}

function getValidMoves (vials: Vials) {
    const validMoves: Move[] = []
    let usedEmptyVial = false
    for (const [i, vialTo] of vials.entries()) {
        if (vialTo.length === 0 && usedEmptyVial) continue
        if (vialTo.length === 0) usedEmptyVial = true
        for (const [j, vialFrom] of vials.entries()) {
            if (i === j) continue
            if (vialFrom.length === 0) continue
            const amount = getAmountTopColor(vialFrom)
            if (vialFrom.length === amount && vialTo.length === 0) continue
            if ((vialTo.length === 0 || vialFrom[0] === vialTo[0] ) && vialTo.length <= (4 - amount)) {
                validMoves.push({ from: j, to: i, amount })
            }
        }
    }
    return validMoves
}

function makeMove (vials: Vials, move: Move) {
    const vialsClone = cloneDeep(vials)
    for (let i = 0; i < move.amount; i++) {
        const color = vialsClone[move.from].shift()
        vialsClone[move.to].unshift(color)
    }
    return vialsClone
}

function isDone (vials: Vials) {
    for (const vial of vials) {
        if(vial.length < 4 && vial.length > 0 || uniq(vial).length > 1) {
            return false
        }
    }
    return true
}

export function solveRecursively (vials: Vials, moves: Move[]) {

    if (isDone(vials)) {
        return moves
    }

    const validMoves = getValidMoves(vials)
    if (!validMoves.length) {
        return false
    }

    for (const move of validMoves) {
        const newConfig = makeMove(vials, move)
        const cloneMoves = cloneDeep(moves)
        cloneMoves.push(move)
        const solution = solveRecursively(newConfig, cloneMoves)
        if (solution) return solution
    }
}