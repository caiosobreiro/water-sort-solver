import { isValid, solveRecursively } from "./helpers"
import { exec } from 'child-process-promise'
import * as adb from 'adbkit'
import * as fs from 'fs-extra'
import * as NodeCache from 'node-cache'

async function main () {

    console.clear()
    console.log('==============================')
    console.log('🧪🧪 Water Sort Game Solver')
    console.log('==============================')

    const client = adb.createClient()
    const devices = await client.listDevices()
    const emulator = devices[0]
    const monkey = await client.openMonkey(emulator.id)

    console.log(`Found ${devices.length} devices:`)
    console.log(devices)
    console.log(`Selected device ${emulator.id}`)

    while (true) {

        // Take a screenshot and save to file
        console.log('Taking screenshot')
        const screencap = await client.screencap(emulator.id)
        await writeStreamToFile(screencap, 'src/tmp/screencap.png')

        const output = await exec(`python3 python/detect_vials_template.py`, { cwd: 'src' })
        // console.log(output.stdout)
        const parsed = JSON.parse(output.stdout)

        const vials = parsed.map(e => e.content)

        console.log(vials)

        if (!isValid(vials)) {
            console.log('Invalid configuration')
            process.exit(1)
        }

        console.log('Entered configuration is valid.')
        console.log('Starting brute force')
        const moves = []
        const solution = solveRecursively(vials, moves)
        if (!solution) {
            console.log('\nNo solution possible ☹️')
            process.exit(1)
        }

        console.log(`Found solution with ${solution.length} moves`)
        console.log(solution)

        for (let [index, move] of solution.entries()) {
            const vialFrom = parsed[move.from]
            const [x1, y1] = vialFrom.position
            const vialTo = parsed[move.to]
            const [x2, y2] = vialTo.position
    
            await new Promise((resolve) => {
                monkey.multi()
                .tap(x1, y1)
                .sleep(300)
                .tap(x2, y2)
                .execute(err => {
                    console.log(`Poured from ${move.from} to ${move.to}`)
                    resolve(0)
                })
            })

            if (index !== solution.length - 1) {
                const nextMove = solution[index + 1]
                if (isNextMoveSafe(move, nextMove)) {
                    console.log('Next move is safe. Skipping sleep')
                    console.log(move, nextMove)
                    continue
                }
            }
            await sleep(2500)
        }

        // click next
        console.log('Clicking next')
        monkey.tap(540, 1620, () => {})

        // console.log('Waiting 12 seconds...')
        // await sleep(15000)
        
        // // close ad

        // // Take a screenshot and save to file
        // const adScreenshot = await client.screencap(emulator.id)
        // await writeStreamToFile(adScreenshot, 'src/ad.png')
        // const outputXButton = await exec(`python3 detectX.py ad.png`, { cwd: 'src' })
        // const Xposition = JSON.parse(outputXButton.stdout)

        // console.log(Xposition)
        // console.log(`Found close button in position ${Xposition}`)
        // monkey.tap(Xposition[0], Xposition[1], () => {})
        // console.log('Closing Ad')

        await sleep(3000)
    }

    // monkey.end()

    // console.log('Finished')
}

async function writeStreamToFile (writable, file) {
    return new Promise((resolve, reject) => {
        const stream = fs.createWriteStream(file)
        writable.pipe(stream)
        stream.on('finish', resolve)
    })
}

function sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time))
}

const cache = new NodeCache()
function isNextMoveSafe (currentMove, nextMove) {
    // nextmove.from equals currentmove.to ?
    if (cache.has(nextMove.from) || cache.has(nextMove.to)) {
        return false
    }
    
    if (currentMove.to === nextMove.to) {
        cache.set(currentMove.from, '', 2.5)
        cache.set(currentMove.to, '', 2.5)
        return true
    }

    if (currentMove.from !== nextMove.from && currentMove.from !== nextMove.to && currentMove.to !== nextMove.from) {
        cache.set(currentMove.from, '', 2.5)
        cache.set(currentMove.to, '', 2.5)
        return true
    }

    return false
}

main().then().catch()