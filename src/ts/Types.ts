export enum Color {
    BLUE = 'BLUE',
    PINK = 'PINK',
    YELLOW = 'YELLOW',
    BROWN = 'BROWN',
    GREEN = 'GREEN',
    LIME = 'LIME',
    PURPLE = 'PURPLE',
    RED = 'RED',
    CYAN = 'CYAN',
    ORANGE = 'ORANGE',
    GREY = 'GREY',
    LIGHTGREEN = 'LIGHTGREEN'
}

export type Vial = Color[]

export type Vials = Vial[]

export type Move = { from: number, to: number, amount: number }